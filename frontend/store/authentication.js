//Axios is used to connect to API
import axios from 'axios';

// state object to save the state of users
const state = ()=> ({
    auth: {},
    authenticated: false,
})

//used to get the current state of an object
const getters = {
    auth: (state)=>state.auth,
    isAuthenticated: (state)=>state.authenticated
}

/*
* global actions that will be performed on the website.
* the login and logout actions are handled here.
* 
* The login action authenticates a username and commits the current state of the authenticated property
* based on the response from the login API
*
* The logout action saves destroys the current state of the user object
*
*/
const actions = {
    async loginUser({commit},payload){
        console.log(payload,'payload')
        const response = await axios.post(process.env.baseUrl+'/users/login',payload)
        console.log(response)
        commit('authenticated',response.data)
    },
    async logoutUser({commit}){
        commit('logout',{})
    }

}

// The mutation method is used to record the change of a state. Like a state update method
const mutations = {
    authenticated: (state, userData) => {
        state.auth = userData
        state.authenticated = true
    },
    logout: (state)=>{
        state.auth = {}
        state.authenticated = false
    }
}

//Export is making all the properties available globally for access.
export default {
    state,
    getters,
    actions,
    mutations
}