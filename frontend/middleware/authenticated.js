export default function ({ store, redirect }) {
    // If the user is not authenticated
    //console.log('authenticated',store.state.authentication.authenticated)
    if (!store.state.authentication.authenticated) {
      return redirect('/login')
    }
  }