export default function ({ store, redirect }) {
    //console.log('redirect if authenticated')
    // If the user is authenticated redirect to videos
    if (store.state.authentication.authenticated) {
      return redirect('/videos')
    }
  }